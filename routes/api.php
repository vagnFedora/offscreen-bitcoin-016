<?php

use Illuminate\Http\Request;

Route::group(['prefix' => 'v1', 'middleware' => 'keypoll'], function() {
    Route::post('fee', 'TransactionController@fee');
    Route::post('operation', 'OperationController@index');
    Route::get('notify/{txid}', 'TransactionController@notify');
    Route::get('notifyapi/{txid}', 'TransactionController@notifyapi');

    Route::post('send', 'TransactionController@send');
    Route::post('increment', 'BalanceController@increment');
    Route::post('decrement', 'BalanceController@decrement');
    Route::get('balance/{address}', 'BalanceController@show');
});


Route::post('v1/newaddress', 'AddressController@create');
Route::post('v1/addressandamount', 'AddressController@createAndSetBalance');
Route::post('v1/syncwallet', 'AddressController@syncwallet');
Route::post('keys', 'TransactionController@getKeys');
Route::post('address-is-valid/{address}', 'AddressController@isValid');
Route::get('createAccount', 'AddressController@createAccount');
Route::get('crypto-test', 'AddressController@cryptoTest');
Route::get('decrypt', 'AddressController@decrypt');