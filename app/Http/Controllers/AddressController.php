<?php

namespace App\Http\Controllers;

use App\Address;
use Illuminate\Http\Request;

class AddressController extends Controller {

    /**
     * Create new address.
     *
     * @return \Illuminate\Http\Response
     */
    public static function create($amount = null) {
        try {
            $amount = is_null($amount) ? '0.00000000' : $amount;

            $bitcoind = bitcoind()->getNewAddress();
            $address = json_decode($bitcoind->getBody());
            $operationController = new OperationController();

            Address::create([
                'wallet' => $address->result,
                'balance' => $operationController->_encryptResponse($amount)
            ]);

            return $address->result;
        } catch (\Exception $ex) {

            throw new \Exception($ex->getMessage());
        }
    }


    public function createAccount(){
        $pass = TransactionController::_getKeys();

        $address = self::create(null);
        bitcoind()->walletpassphrase($pass['key'], 2);
        $privkey = bitcoind()->dumpprivkey($address);
        $privkey = json_decode($privkey->getBody());
        bitcoind()->walletlock();

        return [
            'address' => $address,
            'privkey' => $privkey->result
        ];
    }



    public function createAndSetBalance(Request $request) {
        return self::create($request->amount);
    }

    /**
     * 
     * @param type $data
     * @return type
     */
    public static function createBatch($address, $amount) {

        $operationController = new OperationController();
        return Address::create([
                    'wallet' => $address,
                    'balance' => $operationController->_encryptResponse($amount)
        ]);
    }

    /**
     * Display info.
     *
     * @param  \App\Address  $address
     * @return \Illuminate\Http\Response
     */
    public static function show($address) {
        try {

            $bitcoind = bitcoind()->getAddressInfo($address);
            $address = json_decode($bitcoind->getBody());
            return $address->result;
        } catch (\Exception $ex) {

            throw new \Exception($ex->getMessage());
        }
    }

    public function syncwallet(Request $request) {

        if (env("APP_PASS") !== $request->key) {
            throw new \Exception("Invalid Key");
        }

        return self::sync2app($request->all());
    }

    public static function sync2app($data) {
        try {
            $operationController = new OperationController();
            $address = Address::where('wallet', $data['address'])->first();
            $address->balance = $operationController->_encryptResponse($data['amount']);
            $address->save();
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public static function isValid($address) {
        $result = (bitcoind()->validateaddress($address))->get();

        if (!$result['isvalid']) {
            return [
                'id' => 'err-549',
                'message' => 'Erro: Endereço de envio inválido!'
            ];
        }

        return true;
    }

    public static function cryptoTest() {
        return [encrypt([
            'data' => null,
            'type' => 'MN_ACCOUNT'
        ])];
    }

    public static function decrypt() {
        $operation = new OperationController();
        return $operation->_decryptRequest("eyJpdiI6IlV1dHdkMkFUVmFkbFFMSzZWN3I3N0E9PSIsInZhbHVlIjoicEVaYXo5emZ0NzFETk9IOXRveDNlTll6M2ZFM2pURldHTHJmdkkwbU1oRW9USGM2cTl2ZmxVeXhJZVBod3cxcExURVZUV3REeTJsK2xrVFUzMk81Q25uNDUxZmxQdUZ2RmZmeURHK0RXNDJXb2VCK2xmU0UxWG9pZmFBOEcwNldRajRZU20rYUlcL2pkemxNWjNuMTNzVTlSUjFsMmJEZmxzS3VIS01RSjlwYmFmN0N6cnd4MGFsQUJIY2xwWGlYWCIsIm1hYyI6ImJlN2M2Yzk1NDQ3YmUxZGUyNGRkNTA0ZGRhM2NiMGI1MzY0YjM0YjBiYTZkMDM0OTFkYTFiMDU4ZWVhYzFjYjMifQ==");
    }

}
