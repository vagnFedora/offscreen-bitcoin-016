<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use App\Enum\OperationTypeEnum;

class TransactionController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    public function send(Request $request) {
        return self::create($request->all());
    }

    /**
     *
     *
     * @return \Illuminate\Http\Response
     */
    public static function create($data) {
        try {

            $authenticate = self::_getKeys();
            $transactions = self::prepareMultipleTransaction($data);

            if (!isset($transactions['outputs'])) {
                return [
                    "txid" => null,
                    "errors" => $transactions['errors'],
                    "send" => null,
                    "feeDiff" => $transactions['feeDiff'],
                ];
            }

            $hex = bitcoind()->createrawtransaction($transactions['inputs'], $transactions['outputs']);

            bitcoind()->walletpassphrase($authenticate['key'], 6);

            $signed = (bitcoind()->signrawtransaction($hex->get()))->get();
            $sender = bitcoind()->sendrawtransaction($signed['hex']);

            bitcoind()->walletlock();

            return [
                "txid" => $sender->get(),
                "errors" => $transactions['errors'],
                "send" => $transactions['aproved'],
                "feeDiff" => $transactions['feeDiff'],
            ];
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    public static function getUnspent($transactions) {

        $data = [];
        $data['amount'] = 0;
        $total = 0;
        $_tempValue = 0;

        foreach ($transactions as $t) {
            $total += $t['amount'] + $t['fee'];
        }

        $UTXOs = (bitcoind()->listunspent())->get();

        if (!is_array($UTXOs) || count($UTXOs) === 0) {
            throw new \Exception("Erro: (UTXO) Não há troco ou saldo disponível no momento!");
        }

        foreach ($UTXOs as $utxos) {
            if ($utxos['spendable'] === true) {
                $data['inputs'][] = [
                    'txid' => $utxos['txid'],
                    'vout' => $utxos['vout'],
                    'scriptPubKey' => $utxos['scriptPubKey'],
                    'amount' => $utxos['amount']
                ];

                $data['amount'] += $utxos['amount'];
                if ($data['amount'] >= $total) {
                    break;
                }
            }
        }

        foreach ($transactions as $t) {
            $_tempValue += $t['amount'];

            if ($_tempValue >= $data['amount']) {
                $t['errors'][] = [
                    'id' => 'err-548',
                    'message' => 'Erro: Saldo insuficiente no momento'
                ];
            }

            $data['transactions'][] = $t;
        }

        return $data;
    }

    public static function prepareMultipleTransaction(array $transaction) {
        try {
            $data = [];
            $data['errors'] = null;
            $data['totalToSend'] = 0;
            $data['totalAmount'] = 0;
            $data['feeDiff'] = 0;
            $data['errors'] = [];

            $UTXO = self::getUnspent($transaction);

            foreach ($UTXO['transactions'] as $t) {

                $_testBalance = BalanceController::check($t['fromAddress'], $t['balance']);

                if (is_array($_testBalance)) {
                    $t['errors'][] = $_testBalance;
                }

                $_testAuthenticity = self::_checkAuthenticity($t);
                if (is_array($_testAuthenticity)) {
                    $t['errors'][] = $_testAuthenticity;
                }

                $_address_is_valid = AddressController::isValid($t['toAddress']);
                if (is_array($_address_is_valid)) {
                    $t['errors'][] = $_address_is_valid;
                }

                if (isset($t['errors'])) {
                    $data['errors'][] = $t;
                } else if ($_testBalance === true && $_testAuthenticity === true && $_address_is_valid === true) {
                    $data['outputs'][$t['toAddress']] = sprintf('%.8f', $t['amount']);
                    $data['totalToSend'] += $t['amount'];
                    $data['aproved'][] = $t;
                    $data['feeDiff'] += $t['fee'];
                } else {
                    $data['errors'][] = $t;
                }
            }

            if (isset($data['outputs'])) {
                $rawchangeaddress = (bitcoind()->getrawchangeaddress())->get();
                $rawTransactionTemp = (bitcoind()->createrawtransaction($UTXO['inputs'], $data['outputs']))->get();
                $data['decodedRawTransaction'] = (bitcoind()->decoderawtransaction($rawTransactionTemp))->get();
                $data['fee'] = sprintf('%.8f', ($data['decodedRawTransaction']['size'] * env('FEE_RECOMENDED')) / 100000000);
                $data['outputs'][$rawchangeaddress] = sprintf('%.8f', $UTXO['amount'] - ($data['totalToSend'] + $data['fee']));
                $data['inputs'] = $UTXO['inputs'];
                $data['feeDiff'] = sprintf('%.8f', ($data['feeDiff'] - $data['fee']));
            }

            return $data;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     *
     * @param type $transaction
     * @return type
     * @throws \Exception
     */
    private static function _checkAuthenticity($transaction) {
        try {

            if (!GuzzleController::postOffscreen(OperationTypeEnum::CHECK_AUTHENTICITY, $transaction)) {
                return [
                    'id' => 'err-546',
                    'message' => 'Erro: Autenticidade não comprovada'
                ];
            }

            return true;
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }
    }

    /**
     *
     * @return type
     * @throws \Exception
     */
    public static function _getKeys() {
        $response = GuzzleController::postSign();
        if (!$response) {
            throw new \Exception("Erro ao recuperar a chave de acesso");
        }
        return $response;
    }

    /**
     *
     * @return type
     * @throws \Exception
     */
    public function getKeys() {
        return GuzzleController::postSign();
    }

    /**
     *
     * Assina as transações
     *
     * @param type $hex
     * @param type $unspend
     * @param type $privKey
     * @return type
     */
    private static function signrawtransaction($hex, $unspend, $privKey) {
        return (bitcoind()->signrawtransaction($hex, $unspend, $privKey))->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($txid) {
        //
    }

    /**
     *
     * @return type
     * @throws \Exception
     */
    public function keys() {
        return self::_getKeys();
    }

    /**
     *
     * @param type $txid
     * @return type
     */
    public function notify($txid) {
        try {
            $data = $this->_gettransaction($txid);
            $msg = [];
            if ($data) {
                foreach ($data as $value) {
                    if ($value['category'] === "receive") {
                        $offscreen_type = env('IS_COIN_WARRANTY') ? OperationTypeEnum::NOTIFY_WALLET_WARANTY : OperationTypeEnum::NOTIFY_WALLET;
                        $msg[] = GuzzleController::postOffscreen($offscreen_type, $value);
                    }
                }

            }
            return $msg;
        } catch (\Exception $ex) {

            return [
                'message' => $ex->getMessage(),
                'line' => $ex->getLine(),
                'file' => $ex->getFile()
            ];

        }
    }

    public function notifyapi($txid) {
        return $this->_gettransaction($txid);
    }

    /**
     *
     * @param type $txid
     * @return type
     */
    public function confirmation($txid) {
        $gettransaction = bitcoind()->gettransaction($txid);
        return $gettransaction->get();
    }

    /**
     *
     * @param type $txid
     * @return type
     */
    private function _gettransaction($txid) {
        $gettransaction = bitcoind()->gettransaction($txid);
        $transactionData = $gettransaction->get();
        $data = [];
        foreach ($transactionData['details'] as $transaction) {

            $data[] = [
                'amount' => abs($transaction['amount']),
                'fee' => isset($transactionData['fee']) ? abs($transactionData['fee']) : 0,
                'confirmations' => $transactionData['confirmations'],
                'txid' => $transactionData['txid'],
                'toAddress' => $transaction['address'],
                'category' => $transaction['category'],
                'vout' => $transaction['vout'],
            ];
        }
        return $data;
    }

    /**
     * 
     * @param type $conf_target
     */
    public static function estimateFee($conf_target) {

        $result = 0;

        switch (env("VERSION_CORE")) {
            case "0.15":
                $gettransaction = bitcoind()->estimatefee($conf_target);
                $result = $gettransaction->get();
                break;

            case "0.16":
                $gettransaction = (bitcoind()->estimatesmartfee($conf_target))->get();
                $result = $gettransaction['feerate'];
                break;

            default:
                $result = 0;
                break;
        }

        return $result;
    }

    public static function received() {
        $gettransactions = bitcoind()->listreceivedbyaddress();
        $result = $gettransactions->get();
        return $result;
    }

    public function fee() {
        return self::estimateFee(6);
    }

}
