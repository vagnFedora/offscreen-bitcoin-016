<?php

namespace App\Http\Middleware;

use Closure;

class KeyPool {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        $getwalletinfo = bitcoind()->getwalletinfo();
        $result = $getwalletinfo->get();

        if ((int)$result['keypoolsize'] <= 2) {
            $postSign = \App\Http\Controllers\GuzzleController::postSign();
            bitcoind()->walletpassphrase($postSign['key'], 1);
            bitcoind()->walletlock();
        }
        return $next($request);
    }

}
